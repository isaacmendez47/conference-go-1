import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    content = json.loads(body)

    presenter_name = content["presenter_name"]
    presenter_email = content["presenter_email"]
    title = content["title"]
    subject = "Your presentation has been accepted"
    body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    admin_email = "admin@conference.go"
    send_mail(
        subject,
        body,
        admin_email,
        [presenter_email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    content = json.loads(body)

    presenter_name = content["presenter_name"]
    presenter_email = content["presenter_email"]
    title = content["title"]
    subject = "Your presentation has been rejected"
    body = f"{presenter_name}, we regret to tell you that your presentation {title} has been declined"
    admin_email = "admin@conference.go"
    send_mail(
        subject,
        body,
        admin_email,
        [presenter_email],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="reject_presentation")
        channel.queue_declare(queue="approve_presentation")
        channel.basic_consume(
            queue="reject_presentation",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="approve_presentation",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
